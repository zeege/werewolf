//
// Project werewolf: main.cpp
// Created by alfielin on 2021/08/27.
//

#include <iostream>
#include "werewolf/app.cpp"
#include "picit/app.cpp"

using namespace std;
using namespace nz;

int main () {
    nz::initialize();
//    werewolf::App::run();
    picit::App::run();
    nz::LOOP_THREAD.join();
    return 0;
}

