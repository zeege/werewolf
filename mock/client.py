import json

import requests

headers = {'content-type': "application/json"}
baseurl = "http://47.113.85.53/api/werewolf"


def dopost(data):
    response = requests.post(f"{baseurl}/takeAction", data=json.dumps(data), headers=headers)
    print(response.json())


def jupiter():
    data = {
        "userid": "4",
        "action": 6,
        "targets": ["0", "8"]  # king and prophet
    }
    dopost(data)


def prophet():
    data = {
        "userid": 8,
        "action": 6,
        "targets": ["0"]  # king
    }
    dopost(data)


def wolf():
    data = {
        "userid": "1",
        "action": 6,
        "targets": ["9"]  # villager
    }
    dopost(data)


def witch():
    data = {
        "userid": "7",
        "action": 6,
        "targets": ["no", "9"]
    }
    dopost(data)


def guardian():
    data = {
        "userid": "5",
        "action": 6,
        "targets": []
    }
    dopost(data)


def donext(userid):
    data = {
        "userid": str(userid),
        "action": 5,
        "targets": []
    }
    dopost(data)


def exit_room(userid):
    data = {
        "userid": str(userid)
    }
    response = requests.post(f"{baseurl}/exitRoom", data=json.dumps(data), headers=headers)
    print(response.json())


if __name__ == '__main__':
    print("start")
    # jupiter()
    # prophet()
    # wolf()
    # witch()
    # guardian()
