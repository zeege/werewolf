import requests
import random
import string
from requests_toolbelt.multipart.encoder import MultipartEncoder
import sys
import os

def upload(filepaths):
    pool = string.digits + string.ascii_letters
    userid = 
    secret = 
    salt = ""
    for i in range(0, 120):  # for loop 指定重复次数
        salt += random.choice(pool)
    fields = {
        'fileCount': str(len(filepaths)),
        'serial': 
        'rnd': salt,
        'sig': 
    }
    for i, filepath in enumerate(filepaths):
        if os.path.exists(filepath):
            fields['_' + str(i)] = (filepath, open(filepath, 'rb'), 'application/octect-stream')
        else:
            fields['_' + str(i)] = (filepath, "", 'application/octect-stream')
    data = MultipartEncoder(fields=fields)
    url = 
    headers = {'Content-Type': data.content_type}
    res = requests.post(url=url, data=data, headers=headers)
    print(res)
    jj = res.json()
    print(jj)
    for uurl in jj['data']['urls']:
        print(uurl)


if __name__ == '__main__':
    upload(sys.argv[1:])

