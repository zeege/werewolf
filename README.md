## 引言

所有API都具有如下格式，0表示没问题，其它表示有问题，详见[#错误码]。`baseurl: /api/werewolf/`。

```json
{
  "code": 0,
  "msg": "ok",
  "data": {
    // data
  }
}
```

## API列表

### /createRoom

```json
request: {
  "userid": string,
  "config": [int]  // 长度为11，config[i]表示角色i有几个人，详见下面的角色类型枚举
}
response: {
  "roomid": int
}
```

### /joinRoom

```json
request: {
  "userid": string,
  "roomid": int
}
response: {
  "role": int  // 用户抽到的角色
}
```

### /exitRoom

```json
request: {
  "userid": string
}
response: null
```

### /takeAction

注：由于女巫的特殊性，如果救，`targets=["CURE", id]`；毒，`targets=["POISON", id]`；
什么都不做则为空数组

```json
request: {
  "userid": string,
  "action": int,        // 动作类型枚举量，详见下面
  "targets": [string]   // 动作目标的id，比如预言家要验的id、丘比特要连的id
}
response: {
  "res": [int]          // 动作的结果，目前来说只有预言家会有这个字段，其它都是空数组
}
```

举例：

- 假定`aaa`是丘比特
```json
{
  "userid": "aaa",
  "action": 6,
  "targets": ["bbb", "ccc"]
}
```
- 假定`aaa`是预言家
```json
{
  "userid": "aaa",
  "action": 6,
  "targets": ["bbb"]
}
```

## 枚举量

### 错误码

- -1：未知错误
- [-1000, -2]：服务器内部错误，其实就是HTTP状态码的负数
- -1001：没有此用户
- -1002：没有此房间
- -1003：操作非法

### 角色类型枚举

```c++
enum RoleType{
    VILLAGER = 0,
    PROPHET = 1,
    WITCH = 2,
    HUNTER = 3,
    GUARDIAN = 4,
    IDIOT = 5,
    JUPITER = 6,
    WOLF = 7,
    WOLF_KING = 8,
    WOLF_HUNTER = 9,
};
```

### 动作类型枚举

```c++
enum ActionType {
    VOTE = 0,               // 白天的放逐投票
    SUNSET = 1,             // 天黑请闭眼
    SHOOT = 2,              // 猎人开枪
    WOLF_SUICIDE = 3,       // 狼自爆
    KING_SUICIDE = 4,       // 白狼王自爆
    NIGHT_NEXT = 5,         // 晚上做完动作之后，有一个“下一步”的按钮，点它就POST这个
    NIGHT_ACT = 6,          // 晚上执行动作
};
```


