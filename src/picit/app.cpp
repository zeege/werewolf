//
// Project werewolf: app.cpp
// Created by alfielin on 2021/11/29.
//

#pragma once

#include <hv/HttpServer.h>
#include "router.cpp"

namespace nz {
namespace picit {

class App {
public:

    static void run(int port = 9902, const std::string &baseurl = "/api/picit") {
        init_conf();
        static hv::HttpServer server;
        static auto router = Router::build(baseurl);
        server.registerHttpService(router.get());
        server.port = port;
        server.run(false);
        LOGGER->info("Picit server running @ localhost:{}{}", port, baseurl);
    }

};

}
}
