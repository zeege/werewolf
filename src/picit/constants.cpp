//
// Project werewolf: constants.cpp
// Created by alfielin on 2021/12/02.
//

#pragma once

#include "hv/iniparser.h"

namespace nz {
namespace picit {

std::string SERVER_URL;
std::string SQLITE_DBFILE;
std::string UPLOAD_FILE_ROOT;
std::string UPLOAD_FILE_VIRTUAL_ROOT;

std::string USERID;
std::string SECRET;

constexpr const char *UPLOAD_FILE_COUNT_KEY = "fileCount";
constexpr const char *UPLOAD_FILE_SERIAL_KEY = "serial";
const char *UPLOAD_FILE_IDX_PREFIX = "_";

void init_conf(const std::string &path = "/home/ubuntu/deploy/picit.conf") {
    IniParser conf;
    int code = conf.LoadFromFile(path.c_str());
    LOGGER->info("code {}, url {}", code, conf.GetValue("url"));
    SERVER_URL = conf.GetValue("url");
    SQLITE_DBFILE = conf.GetValue("filepath");
    UPLOAD_FILE_ROOT = conf.GetValue("abs_root");
    UPLOAD_FILE_VIRTUAL_ROOT = HPath::join(SERVER_URL, conf.GetValue("virtual_root"));
    USERID = conf.GetValue("userid");
    SECRET = conf.GetValue("secret");
}


}
}

