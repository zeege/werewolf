//
// Project werewolf: router.cpp
// Created by alfielin on 2021/11/29.
//

#pragma once

#include "hv/HttpService.h"
#include "filter_chain_builder.cpp"
#include "api.cpp"
#include "util/basic_filter.cpp"
#include <memory>

namespace nz {
namespace picit {

class Router {
public:

    static std::shared_ptr<hv::HttpService> build(const std::string &baseurl) {
        // preprocessor=>processor=>errorHandler=>postprocessor
        auto router = std::make_shared<hv::HttpService>();
        router->base_url = baseurl;
        router->preprocessor = BasicHandler::preprocessor(FilterChainBuilder::filter);
        router->errorHandler = BasicHandler::error_handler(LOGGER);
        router->postprocessor = BasicHandler::postprocessor();
        router->GET("/hello", api::hello_world);
        router->POST("/upload", api::upload);
        router->GET("/upload", api::upload);
        BasicHandler::guard_with_exception_handler(LOGGER, router);
        return router;
    }

};

}
}
