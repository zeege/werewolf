//
// Project werewolf: filter_chain_builder.cpp
// Created by alfielin on 2021/11/29.
//

#pragma once

#include "constants.cpp"
#include "util/sha1.cpp"
#include "util/basic_filter.cpp"
#include "util/http_filter_chain.cpp"

namespace nz {
namespace picit {

class FilterChainBuilder {
public:

    static int filter(HttpRequest *req, HttpResponse *resp) {
        return build().filter_next(req, resp);
    }

private:

    static HttpFilterChain build() {
        static FilterSequence sequence({
                                               { BasicHandler::basic_filter, true },
                                               { authenticate,               true }
                                       });
        HttpFilterChain chain(&sequence);
        return chain;
    }

    static int authenticate(HttpRequest *req, HttpResponse *resp, HttpFilterChain *chain) {
        std::string rnd = req->GetString("rnd");
        std::string sig = req->GetString("sig");
        std::string realsig = SHA1()(USERID + rnd) + SHA1()(SECRET + rnd);
        if(sig == realsig){
            return HTTP_STATUS_UNFINISHED;
        } else {
            return HTTP_STATUS_FORBIDDEN;
        }
    }

};

}
}


