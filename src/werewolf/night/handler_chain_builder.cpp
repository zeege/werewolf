//
// Project werewolf: chain_builder.cpp
// Created by alfielin on 2021/11/13.
//

#pragma once

#include "werewolf/game/room_context.cpp"
#include "wakeup_handler.cpp"
#include "sunrise_handler.cpp"

namespace nz {
namespace werewolf {

class RoomContext;

class HandlerChainBuilder {
public:

    HandlerChainBuilder() = delete;

    static std::shared_ptr<HandlerChain> build(const GameConfig &config, RoomContext *context) {
        auto chain = std::make_shared<HandlerChain>();
        static const std::vector<int> order = { int(RoleT::JUPITER),
                                                int(RoleT::PROPHET),
                                                int(RoleT::WOLF),
                                                int(RoleT::WITCH),
                                                int(RoleT::GUARDIAN),
                                                int(RoleT::HUNTER),
                                                int(RoleT::SUNRISE) };
        for (int role: order) {
            if (config.v.at(role) != 0) {
                auto handler = std::make_shared<WakeupHandler>(RoleT(role), context);
                chain->add_handler(handler);
            }
        }
        auto handler = std::make_shared<SunriseHandler>(RoleT::SUNRISE, context);
        chain->add_handler(handler);
        return chain;
    }

private:

};

}
}
