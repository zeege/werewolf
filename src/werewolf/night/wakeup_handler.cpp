//
// Created by alfielin on 2021/11/12.
//

#pragma once

#include "handler.cpp"

namespace nz {
namespace werewolf {

class WakeupHandler : public HandlerBase {
private:
    int timeout = 20000;
public:

    WakeupHandler() = default;

    WakeupHandler(RoleT role, RoomContext *context) : HandlerBase(role, context) {
        if (role == RoleT::WOLF) {
            timeout = 40000000;
        } else {
            timeout = 20000000;
        }
    }

    void timeout_callback() {
        this->chain.lock()->handle_next(role);
    }

    void handle() override {
        LOGGER->debug("Handling {}...", get_name());
        LOOP->setTimeout(timeout, [&](auto id) { this->timeout_callback(); });
        if (role == RoleT::WITCH) {
            if (!context->used_cure) {
                std::string victim = "no victim";
                if (!context->night[int(RoleT::WOLF)].empty()) {
                    victim = context->night[int(RoleT::WOLF)][0];
                }
                LOGGER->debug("Inform witch that victim is {}", victim);
            }
        }
        if (role == RoleT::HUNTER) {
            LOGGER->debug("Shoot status: {}", context->can_shoot());
        }
    }

};

}
}
