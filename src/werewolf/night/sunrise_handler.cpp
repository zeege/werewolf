//
// Project werewolf: sunrise_handler.cpp
// Created by alfielin on 2021/11/14.
//

#pragma once

#include "handler.cpp"

#include <utility>

namespace nz {
namespace werewolf {

class SunriseHandler : public HandlerBase {
public:

    SunriseHandler() = default;

    explicit SunriseHandler(RoleT role, RoomContext *context) : HandlerBase(role, context) {
    }

    void handle() override {
        LOGGER->debug("Handling SUNRISE...");
        context->commit_night();
    }

};

}
}

