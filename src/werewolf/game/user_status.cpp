//
// Project werewolf: user_status.cpp
// Created by alfielin on 2021/11/13.
//

#pragma once

#include <string>
#include <utility>
#include "werewolf/constants.cpp"

namespace nz {
namespace werewolf {

class UserStatus {
private:
    bool alive = true;

public:
    std::string userid;
    size_t index = 0;
    int role = 0;
    bool metamorphosed = false;
    bool have_will = true;
    UserStatus *lover = nullptr;

    UserStatus() = default;

    UserStatus(std::string userid, size_t index, int role) :
            userid(std::move(userid)), index(index), role(role), metamorphosed(false), alive(true) {}

    void kill(bool can_have_will, bool can_metamorphose) {
        have_will = can_have_will;
        metamorphosed = can_metamorphose;
        if ((role != int(RoleT::IDIOT)) || !can_metamorphose) {
            alive = false;
        }
        if (lover != nullptr) {
            lover->kill(can_have_will, can_metamorphose);
        }
    }

    void clear() {
        metamorphosed = false;
        alive = true;
        have_will = true;
        lover = nullptr;
    }

    bool is_alive() const { return alive; }

    bool is_wolf() const {
        return role >= int(RoleT::WOLF);
    }
};

}
}
