//
// Project werewolf: naive_voter.cpp
// Created by alfielin on 2021/11/15.
//

#pragma once

#include <functional>
#include <string>
#include <vector>

namespace nz {
namespace werewolf {

class FakeVoter {
private:
    std::atomic_flag cb_once_flag{};
    std::function<void(std::vector<std::string>)> callback;

public:

    FakeVoter() = default;

    void init(const std::vector<std::string> &userids,
              std::function<void(std::vector<std::string>)> cb){
        callback = std::move(cb);
    }

    void vote(const std::string &from, const std::string &to) {
        if (!this->cb_once_flag.test_and_set()) {
            callback({ to });
        }
    }

    void clear() {
        cb_once_flag.clear();
    }

};

}
}

