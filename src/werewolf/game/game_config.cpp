//
// werewolf: game_config.cpp
// Created by alfielin on 11/13/2021.
//

#pragma once

#include <vector>
#include "werewolf/constants.cpp"

namespace nz {
namespace werewolf {

class GameConfig {
public:
    std::vector<int> v;

    GameConfig() {
        v.resize(int(RoleT::UGLY_COUNT_), 0);
    }

    explicit GameConfig(const std::vector<int> &conf) {
        v = conf;
    }

    int required_users_num() {
        int ret = 0;
        for (auto i: v) { ret += i; }
        return ret;
    }

    void set_role(RoleT role_type, int count) {
        v[int(role_type)] = count;
    }

    void reset() {
        v.clear();
        v.resize(int(RoleT::UGLY_COUNT_), 0);
    }

};

}
}
