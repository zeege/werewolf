//
// Project werewolf: filter_chain_builder.cpp
// Created by alfielin on 2021/11/17.
//

#pragma once

#include "util/http_filter_chain.cpp"
#include "util/basic_filter.cpp"
#include "util/sha1.cpp"

namespace nz {
namespace werewolf {

class FilterChainBuilder {
public:

    static int filter (HttpRequest *req, HttpResponse *resp) {
        return build().filter_next(req, resp);
    }

private:

    static HttpFilterChain build () {
        static FilterSequence sequence({
                                               { BasicHandler::basic_filter, true },
                                               { wxauth,                     true }
                                       });
        HttpFilterChain chain(&sequence);
        return chain;
    }

    static int wxauth (HttpRequest *req, HttpResponse *resp, HttpFilterChain *chain) {
//        std::string userid = req->json.at("userid");
//        std::string sig = req->json.at("sig");
//        std::string rnd = req->json.at("rnd");
        return HTTP_STATUS_UNFINISHED;
    }

};

}
}


