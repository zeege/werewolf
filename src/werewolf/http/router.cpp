//
// Project werewolf: router.cpp
// Created by alfielin on 2021/11/16.
//

#pragma once

#include "hv/HttpService.h"
#include "filter_chain_builder.cpp"
#include "api.cpp"

namespace nz {
namespace werewolf {

class Router {
public:

    static std::shared_ptr<hv::HttpService> build (const std::string &baseurl) {
        // preprocessor=>processor=>errorHandler=>postprocessor
        auto router = std::make_shared<hv::HttpService>();
        router->base_url = baseurl;
        router->preprocessor = BasicHandler::preprocessor(FilterChainBuilder::filter);
        router->errorHandler = BasicHandler::error_handler(LOGGER);
        router->postprocessor = BasicHandler::postprocessor();
        router->GET("/hello", api::hello_world);
        router->POST("/createRoom", api::create_room);
        router->POST("/joinRoom", api::join_room);
        router->POST("/exitRoom", api::exit_room);
        router->POST("/takeAction", api::take_action);
        router->GET("/nightDeath", api::get_night_death_list);
        BasicHandler::guard_with_exception_handler(LOGGER, router);
        LOGGER->info("Built router with base url [{}]", router->base_url);
        return router;
    }

};

}
}
