//
// Project werewolf: app.cpp
// Created by alfielin on 2021/11/17.
//

#pragma once

#include "http/router.cpp"
#include "hv/HttpServer.h"

namespace nz {
namespace werewolf {

class App {
public:

    static void run (int port = 9901, const std::string &baseurl = "/api/werewolf") {
        static hv::HttpServer server;
        static auto router = Router::build(baseurl);
        server.registerHttpService(router.get());
        server.port = port;
        server.run(false);
        LOGGER->info("Werewolf server running @ localhost:{}{}", port, baseurl);
        test();
    }

    static void test(){
#ifdef NZDEBUG_WEREWOLF
        auto &manager = RoomManager::get();
        GameConfig config;
        config.set_role(RoleT::PROPHET, 1);
        config.set_role(RoleT::WITCH, 1);
        config.set_role(RoleT::HUNTER, 1);
        config.set_role(RoleT::GUARDIAN, 1);
        config.set_role(RoleT::JUPITER, 1);
        config.set_role(RoleT::WOLF, 3);
        config.set_role(RoleT::WOLF_KING, 1);
        config.set_role(RoleT::VILLAGER, 4);
        int roomid = manager.create_room("0", config);
        auto room = manager.get_room(roomid);
        for (int i = 1; i <= 12; ++i) {
            std::string username = std::to_string(i);
            room->join(username);
        }
        room.reset();
        manager.start_game(roomid);
#endif
    }

};

}
}

