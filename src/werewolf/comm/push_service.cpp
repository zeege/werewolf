//
// Project werewolf: push_service.cpp
// Created by alfielin on 2021/11/14.
//

#pragma once

namespace nz {
namespace werewolf {

class PushService {

    virtual void push() = 0;

};

}
}


