//
// Project werewolf: constants.cpp
// Created by alfielin on 2021/11/20.
//
#pragma once


#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

namespace nz {

#define ENABLE_SINGLETON(cls) \
public:                                     \
    cls(const cls&) = delete;               \
    cls& operator=(const cls&) = delete;    \
    static cls &get() {                     \
        static cls singleton;               \
        return singleton;                   \
    }                         \
private:


#define NZ_BEGIN_CRITICAL(mtx) { std::lock_guard<std::mutex> guard(mtx);
#define NZ_END_CRITICAL }

std::shared_ptr<spdlog::logger> LOGGER;
hv::EventLoopThread LOOP_THREAD;
const hv::EventLoopPtr &LOOP = LOOP_THREAD.loop();

void initialize(spdlog::level::level_enum level = spdlog::level::debug) {
    if (level == spdlog::level::debug) {
        hlog_enable_fsync();
    }
    LOGGER = spdlog::stdout_color_mt("werewolf");
    LOGGER->set_pattern("[%Y-%m-%d %H:%M:%S t%t][%^%l%$]%@ %v");
    LOGGER->set_level(level);
    LOOP_THREAD.start();
    LOOP->runInLoop([]() {
        LOGGER->info("Event loop running @ pid: {}, tid: {}", hv_getpid(), hv_gettid());
    });
    LOOP->setInterval(60 * 1000, [](auto timer_id) {
        LOGGER->info("Heartbeat");
    });
    LOGGER->info("Application boosted @ pid: {}, tid: {}", hv_getpid(), hv_gettid());
}

}
