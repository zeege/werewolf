//
// Project werewolf: optional.cpp
// Created by alfielin on 2021/12/05.
//
#pragma once

#include <stdexcept>

template<typename T>
class Optional {
private:
    bool _exists = false;
    T _data;

    void check() {
        if (!_exists) {
            throw std::logic_error(fmt::format("Optional {} has no data", typeid(T).name()));
        }
    }

public:
    Optional() = default;

    explicit Optional(T data) : _data(data), _exists(true) {
    }

    T &operator()() {
        check();
        return _data;
    }

    explicit operator T() {
        check();
        return _data;
    }

    explicit operator bool() { return _exists; }
};